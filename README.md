# JavaPro : Un conteneur d'injection de dépendances
Pour répondre aux User Stories, plusieurs implémentations sont possibles.
Nous expliquons ici la façon dont nous avons choisie d'implémenter ces différentes fonctionnalités.
Nous avons créé une classe **Injecteur** qui gère les injections, il faut tout d'abord l'initialiser afin de l'utiliser :
``` java
// Création d'un injecteur
Injector injector = new Injector();
```
## US0: Objects and values
Le conteneur que nous avons développé permet d'injecter des objets à une interface, une classe, ou un objet.
L'injection d'une classe ou d'une interface se fait de la manière suivante :
- Association entre l'interface et son implémentation
- Création d'une nouvelle instance et injection de l'implémentation
``` java
// Association de l'interface et de son implémentation
injector.register(MonInterface.class,MonImplementation.class);

// Injection de l'implémentation
MonInterface monObjet = injector.newInstance(MonInterface.class);
```

L'injection d'une valeur se fait de manière similaire :
``` java
// Association de la classe et de sa valeur
injector.register(int.class,3);

// Injection de la valeur
int maValeur = injector.newInstance(int.class);
```

**Notes :**
- Si aucune implémentation n'est définie, alors l'erreur **NoMatchingFoundException** est levée.
- Si l'utilisateur cherche à implementer une classe abstraite, l'exception **AbstractClassException** est levée.

## US1: Setter injection

Nous avons choisi de faire fonctionner l'injection par setter grâce à une annotation **@InjectSetter**.
L'utilisateur indique au conteneur si un champ doit être injecté par son setter en mettant cette annotation sur le setter. Le conteneur va associer champ et setter par leurs noms, il faut donc que le nom du setter respecte le format **setNomDuChampDansLaClasse**.
```java
public class MaClasse{
    private int valeurAInjecter;
    private MonInterface objetAInjecter;
    
    @InjectSetter
    public void setValeurAInjecter(int valeurAInjecter){
        this.valeurAInjecter= valeurAInjecter;
    }
    
    @InjectSetter
    public void setObjetAInjecter(int objetAInjecter){
        this.objetAInjecter= objetAInjecter;
    }
}
```
Par la suite, le conteneur instancie une instance du type indiqué en paramètre du setter et appelle la méthode.

## US2: Constructor injection

Pour l'injection par constructeur, l'utilisateur indique au conteneur si un constructeur doit être appelé afin d'injecter l'ensemble de ses attributs via l'annotation **@InjectConstructor**.
```java
public class MaClasse{
    private int valeurAInjecter;
    private MonInterface objetAInjecter;

    // Définition d'un constructeur sans paramètres pour remplacer l'absence du constructeur par défaut
    public MaClasse(){
    }

    // Constructeur pour l'injection
    @InjectConstructor
    public MaClasse(int valeurAInjecter, MonInterface objetAInjecter){
        this.valeurAInjecter = valeurAInjecter;
        this.objetAInjecter = objetAInjecter;
    }
}
```
Par la suite, le conteneur instancie une instance pour chaque paramètre du constructeur et appelle la méthode.

**Notes :**
-  Il faut redéfinir un constructeur sans paramètres dans la classe, car la classe ayant déjà un constructeur, le constructeur habituel par défaut ne sera pas créé. Si ce constructeur n'a pas été défini l'erreur **NoConstructorWithNoParametersFoundException** est levée.
- A ce stade on peut envisager avoir un mélange d'injection par setter et par constructeur. Nous avons développé le conteneur afin qu'il essaye d'injecter par setter puis par constructeur si cela est possible. A chaque injection, le conteneur injecte un objet que si ce dernier n'a pas déjà été injecté (si non null). En revanche, une valeur est toujours injectée même si elle a déjà été initialisée par une précédente injection. En effet, une valeur non initialisée a une valeur par défaut non différentiable d'une valeur initialisée.
- Lorsque plusieurs constructeurs possèdent l'annotation **@InjectConstructor**, l'erreur **MultipleInjectConstructorsFoundException** est levée.

## US3: Field injection

L'utilisateur indique au conteneur si un champ doit être injecté via l'annotation @InjectField.
``` java
public class MaClasse{
    @InjectField
    private int valeurAInjecter;

    @InjectField
    private MonInterface objetAInjecter;
}
```
Le conteneur instancie ensuite directement une instance de l'objet et l'injecte dans l'attribut ciblé.

## US4: Dependency Resolution (binding)

L'association entre les éléments se fait par le biais d'une map (registry) permettant d'associer par exemple une interface à son implémentation.
``` java
public class Injector {
    // Le map d'association entre abstractions et implémentations
    static Map<Class<?>, List<Object>> registry = new HashMap<>();

    // Constructeur de l'injecteur
    public Injector() {}
    }
```
Pour associer deux éléments de son projet, le développeur doit, par exemple, utiliser la ligne suivante.
``` java
Injector.register(Interface.class, Implementation.class);
```
Par la suite, lorsque le conteneur injecte un élément du type de l'interface, il cherchera son association dans la map.
**Notes :**
- Si aucune association n'a été définie au préalable l'erreur **NoMatchingFoundException** est levée.
- Si l'utilisateur cherche à faire deux fois exactement la même association interface-implémentation, l'erreur **AlreadyBindException** est levée.

## US5: Support Singleton

Une classe est désignée comme singleton pour le conteneur lorsqu'il possède l'annotation @Singleton sur la définition de sa classe.
``` java
@Singleton
public class MaClasse{
}
```
Lorsque cela est le cas et que cette classe a besoin d'être injectée le **SingletonInjector** détermine si celle-ci a déjà été instanciée et si c'est le cas retourne alors l'instance sauvegardée sinon le conteneur crée une nouvelle instance et la sauvegarde pour de future utilisation.

## US6: Multiple Implementations

Plusieurs implémentations peuvent être déclarées pour une même interface dans la map du conteneur.
Pour permettre au conteneur de choisir l'implémentation à instancier, une des implémentations doit posséder l'annotation **@Default**, celle-ci sera donc instanciée pour chaque instance de l'interface.
``` java
public interface MonInterface{
}

@Default
public class MaClasse1 implements MonInterface{
    // Cette classe sera instanciée par défaut
}

public class MaClasse2 implements MonInterface{
}

public class MaClasse3 implements MonInterface{
}
```
**Notes :**
- Si une unique implémentation est définie pour une interface, celle-ci est prise par défaut et aucune annotation n'est nécessaire.
- S'il existe plusieurs implémentations et aucunes ne portant l'annotation **@Default** l'erreur **MultiplteImplFoundException** est levée.
- Si plusieurs implementations ayant l'annotation **@Default** sont définies, alors l'erreur **MultiplteDefaultImplFoundException** est levée.
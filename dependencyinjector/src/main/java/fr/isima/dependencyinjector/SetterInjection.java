package fr.isima.dependencyinjector;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fr.isima.dependencyinjector.annotations.InjectSetter;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;

/**
 * Class used to set injection by setters
 *
 */
public class SetterInjection {
	
	/**
	 * Inject attributes of an instance if they have an annotated setter
	 * @param instance, in which injection done if possible, otherwise the instance as it was in input
	 * @throws NoMatchingFoundException 
	 * @throws MultiplteDefaultImplFoundException 
	 * @throws MultiplteImplFoundException 
	 * @throws NoConstructorWithNoParametersFoundException 
	 * @throws MultipleInjectConstructorsFoundException 
	 * @throws AbstractClassException 
	 */
	static void injectBySetter(Object instance) throws NoMatchingFoundException, MultiplteImplFoundException, 
	MultiplteDefaultImplFoundException, NoConstructorWithNoParametersFoundException, 
	MultipleInjectConstructorsFoundException, AbstractClassException {
		System.out.println("go in setter injection");
		if (instance != null) {
	        // List of setters 
	        List<Method> methods = findSetters(instance.getClass());
	        
	        for (Method method : methods) {
	        	
	        	// If the attribute needs to be injected
	        	if(isPrimitive(instance, method) || isAttributeNotAlreadyInjected(instance,method)) {
	        		
	        		// A setter has always only one parameter
		        	Class<?>[] types = method.getParameterTypes();
		        	
		        	try {
		        		System.out.println("creation of an instance of " + types[0]);
						Object attributeCreated = Injector.newInstance(types[0]);
						
						System.out.println("invoke setter " + method.getName());
						method.invoke(instance, attributeCreated);				
					} catch ( IllegalAccessException | InvocationTargetException e) {
						throw new NoMatchingFoundException();
					}
	        	}
	        }
	    }
	}
	
	
   /**
    * Recover all setters of a class
    * @param c, the class to scan
    * @return the list of setters corresponding to the class
    */
   static ArrayList<Method> findSetters(Class<?> c) {
	   System.out.println("Looking for setters on instance of " + c.getSimpleName());
	   ArrayList<Method> list = new ArrayList<Method>();
	   // Get all method of the class to find setters
	   Method[] methods = c.getDeclaredMethods();
	   for (Method method : methods) {
	      if (isSetter(method) && method.isAnnotationPresent(InjectSetter.class)) {
	         list.add(method);
	         System.out.println("setter found: " + method.getName());
	      }
	   }
       if (methods.length == 0) {
            System.out.println("No setters found on type " + c.getSimpleName());
       }
       return list;
	}
   
   
   /**
    * Test if a method is a setter by its name
    * @param method, to be checked if it's a setter
    * @return true if the method is a setter, otherwise false 
    */
   public static boolean isSetter(Method method) {
	   // If method is public, return nothing, has only one parameter and the name begin by "set"
	   return Modifier.isPublic(method.getModifiers()) &&
			   					method.getReturnType().equals(void.class) &&
			   					method.getParameterTypes().length == 1 &&
			   					method.getName().matches("^set[A-Z].*");
   }
   
   /**
    * Get the attribute name from its setter
    * @param setter of the attribute
    * @return the name of the attribute
    */
   public static String getAttributeName(Method setter) {
	   String attributeName = setter.getName().substring(4, setter.getName().length());
	   return Character.toLowerCase(setter.getName().charAt(3)) + attributeName;
   }
   
   /**
    * Test if an attribute is a primitive
    * @param instance, the instance containing the attribute
    * @param setter, the setter of the attribute
    * @return true if the attribute is a Primitive, and false otherwise
    */
   public static boolean isPrimitive(Object instance, Method setter) {
	   String attributeName = getAttributeName(setter);
	   Field field = null;
	   try {
		    field = instance.getClass().getDeclaredField(attributeName);
		} catch (  NoSuchFieldException  e) {
			e.printStackTrace();
		}
	   return field.getClass().isPrimitive();
   }
   
   /**
    * Test if an attribute has already been injected
    * @param instance, the instance containing the attribute
    * @param setter, the setter corresponding to the attribute
    * @return true if the attribute has not been injected and false otherwise
    */
   public static boolean isAttributeNotAlreadyInjected(Object instance, Method setter) {
	   // Get the attribute name
	   String attributeName = getAttributeName(setter);
   
	   Object attribute = null;
	   try {
			Field field = instance.getClass().getDeclaredField(attributeName);
			field.setAccessible(true);
			attribute = field.get(instance);
		} catch ( IllegalAccessException | NoSuchFieldException  e) {
			e.printStackTrace();
		}

	   return attribute == null;
   }
}

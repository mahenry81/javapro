package fr.isima.dependencyinjector;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;

/**
 * Class used to map abstract object and its implementation 
 *
 */
public class Injector {
	
	/**
	 * The mapping between abstractions and implementations
	 */
	static Map<Class<?>, List<Object>> registry = new HashMap<>();
	
	/**
	 * Constructor of the injector class
	 */
	public Injector() {}	
	
	/**
	 * Create new value
	 * @param value, to create
	 * @return the casted value if not null and NoMatchingFoundException otherwise
	 * @throws NoMatchingFoundException
	 */
	@SuppressWarnings("unchecked")
	private static <T> T newValue(Object value) throws NoMatchingFoundException {
		if (value != null) {
			return (T) value;
		} else {
			throw new NoMatchingFoundException();
		}
	
	}
	
	/**
	 * Create a new instance of object
	 * @param classToGet, the class to instantiate
	 * @return the new instance if succeed or an error otherwise
	 * @throws MultiplteImplFoundException
	 * @throws NoMatchingFoundException
	 * @throws MultiplteDefaultImplFoundException
	 * @throws NoConstructorWithNoParametersFoundException
	 * @throws MultipleInjectConstructorsFoundException
	 * @throws AbstractClassException 
	 */
	public static <T> T newInstance(Class<T> classToGet) throws MultiplteImplFoundException, NoMatchingFoundException, 
	MultiplteDefaultImplFoundException, NoConstructorWithNoParametersFoundException, 
	MultipleInjectConstructorsFoundException, AbstractClassException  {
		System.out.println("new instance of " + classToGet.getSimpleName());
			// If classToGet is an interface or a value search value in registry
			if (classToGet.isInterface()) {
				List<Object> instance = registry.get(classToGet);
				// Class to inject
				if (instance != null) {
					return ClassInjector.newImplInstance(instance);
				} else {
					throw new NoMatchingFoundException();
				}
			} else { 
				// If classToGet is a class
				if (!classToGet.isPrimitive()) {
					if (!(Modifier.isAbstract( classToGet.getModifiers()))) { // if it's not an abstract class
						return ClassInjector.newClassInstance(classToGet);		
					} else {
						throw new AbstractClassException();
					}
				} else { 
					//  If classToGet is a value
					Object value = registry.get(classToGet).get(0);
					if (value != null) {
						// If the first element is a class, the rest is too
							return newValue(value);
					} else {
						throw new NoMatchingFoundException();
					}
				}
			}
		
	}

	/**
	 * Match a interface with his implementation
	 * @throws AlreadyBindException 
	 */
	public static <T1, T2> void register(Class<T1> classAbstract, T2 impl) throws AlreadyBindException {
		List<Object> impls = registry.get(classAbstract);
		if (impls == null) {
			impls = new ArrayList<>();
			impls.add(impl);
			registry.put(classAbstract, impls);		
		} else { 
			// Other implementation has already been saved
			for (Object i : impls) {
				if (i.equals(impl)) throw new AlreadyBindException();
			}
			impls.add(impl);
		}
		
	}
}

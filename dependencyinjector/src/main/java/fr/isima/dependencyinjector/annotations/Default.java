package fr.isima.dependencyinjector.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation used to defined a default implementation in cases where there are multiple possible implementations for a same abstraction
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Default {}

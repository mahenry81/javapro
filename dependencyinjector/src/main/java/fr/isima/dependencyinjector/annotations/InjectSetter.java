package fr.isima.dependencyinjector.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to defined a specific setter to be used to do injection
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectSetter {

}

package fr.isima.dependencyinjector.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation used to defined a specific injection by Field
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectField {

}

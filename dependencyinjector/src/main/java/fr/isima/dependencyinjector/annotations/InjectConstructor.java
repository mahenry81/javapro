package fr.isima.dependencyinjector.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation used to defined an injection by a specified constructor
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectConstructor {

}

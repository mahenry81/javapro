package fr.isima.dependencyinjector.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation used to defined that the specific class has to be manage by the container as a singleton
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Singleton {

}

package fr.isima.dependencyinjector;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used for injection by Singleton
 *
 */
public class SingletonInjector {
	
	static Map<Class<?>, Object > singletons = new HashMap<>();
	
	private SingletonInjector() {}
	
	/**
	 * Return a new instance or a saved instance of a class which is a singleton
	 * 
	 * @param classToInject, the class needed to be injected
	 * @return the instance needed
	 */
	@SuppressWarnings("unchecked")
	static <T> T newSingletonInstance(Class<?> classToInject) {
		Object instance = singletons.get(classToInject);
		
		// The singleton has never been instantiate
		if (instance == null) {
			try {
				T instanceCreated = (T) Class.forName(classToInject.getName()).newInstance();
				singletons.put(classToInject, instanceCreated);
				return instanceCreated;
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else { // The singleton has already been instantiate, return the saved instance
			return (T) instance;
		}
		return null;
	}
}

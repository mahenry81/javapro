package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that multiple constructors are defined with the annotation @InjectConstructor
 *
 */
@SuppressWarnings("serial")
public class MultipleInjectConstructorsFoundException extends Exception {

}

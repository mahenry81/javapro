package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that multiple default Implementation has been defined with the annotation @Default
 *
 */
@SuppressWarnings("serial")
public class MultiplteDefaultImplFoundException extends Exception{
}

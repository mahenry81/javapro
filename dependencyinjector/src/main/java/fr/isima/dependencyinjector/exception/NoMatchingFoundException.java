package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that no implementation of the abstraction has been found
 *
 */
@SuppressWarnings("serial")
public class NoMatchingFoundException extends Exception{

}

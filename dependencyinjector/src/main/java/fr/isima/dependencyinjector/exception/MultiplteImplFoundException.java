package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that multiple implementation has been defined
 *
 */
@SuppressWarnings("serial")
public class MultiplteImplFoundException extends Exception{
}

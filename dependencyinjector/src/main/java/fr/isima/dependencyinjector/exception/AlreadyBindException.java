package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that the association interface implementation has already been made
 *
 */
@SuppressWarnings("serial")
public class AlreadyBindException extends Exception {

}

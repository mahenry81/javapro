package fr.isima.dependencyinjector.exception;

/**
 * Exception used to prevent that a abstract class can't be injected
 *
 */
@SuppressWarnings("serial")
public class AbstractClassException extends Exception {

}

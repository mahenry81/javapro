package fr.isima.dependencyinjector.exception;

/**
 * This exception is used to prevent that there are no constructors without parameters defined but injection by constructor is asked
 * 
 */
@SuppressWarnings("serial")
public class NoConstructorWithNoParametersFoundException extends Exception {

}

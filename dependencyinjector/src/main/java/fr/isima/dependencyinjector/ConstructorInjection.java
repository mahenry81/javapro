package fr.isima.dependencyinjector;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import fr.isima.dependencyinjector.annotations.InjectConstructor;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;

/**
 * Class used to do injection by constructors
 *
 */
public class ConstructorInjection {

	/**
	 * Inject attributes of an instance if they have a annotated constructor
	 * @param instance to inject
	 * @return the injected instance if possible, otherwise the instance as it was in input
	 * @throws NoMatchingFoundException
	 * @throws NoConstructorWithNoParametersFoundException 
	 * @throws MultiplteDefaultImplFoundException 
	 * @throws MultipleInjectConstructorsFoundException 
	 * @throws AbstractClassException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	static <T> T injectByConstructor(Object instance) throws NoMatchingFoundException, NoConstructorWithNoParametersFoundException,  
	MultipleInjectConstructorsFoundException, AbstractClassException {
		System.out.println("go in constructor injection");
		if(instance != null) {
			try {
				// List of constructors
				List<Constructor> constructors = findConstructor(instance.getClass());
				if(!constructors.isEmpty()) {
					// Get constructor 
			        Constructor constructor = constructors.get(0);
			        System.out.println("constructor find : " + constructor.getName());
	        		
	        		// Gets the parameters of the constructor
		        	Class<?>[] parameters = constructor.getParameterTypes();
		        	Object[] parameterstoInject = new Object[parameters.length];
		        	
		        	for(int i=0; i<parameters.length; i++) {
		        		
		        		// If the attribute needs to be injected
			        	if(parameters[i].isPrimitive() ||isAttributeNotAlreadyInjected(instance,parameters[i])) {
			        		System.out.println("creation of an instance of " + parameters[i]);
			        		Object o = Injector.newInstance(parameters[i]);     		
			        		parameterstoInject[i] = o ;
			        	}else {
			        		Field field = instance.getClass().getDeclaredField(getAttributeName(parameters[i]));
							// Change visibility to access to its implementation
							field.setAccessible(true);
							Object attribute = field.get(instance);
			        		parameterstoInject[i] = attribute;
			        	}
		        	}
		        	System.out.println("invoke constructor " + constructor.getName());
		        	instance = constructor.newInstance(parameterstoInject);
				}
			} catch (IllegalAccessException |  InvocationTargetException | MultiplteImplFoundException | 
					MultiplteDefaultImplFoundException | NoSuchFieldException |  InstantiationException e ) {
				System.out.println(e);
				if(e instanceof MultipleInjectConstructorsFoundException) throw new MultipleInjectConstructorsFoundException();
				else throw new NoMatchingFoundException();
			}
		}
		return (T) instance;
	}
	
	/**
	 * Get the attribute name from the class parameter
	 * @param parameter, the class parameter of the constructor
	 * @return the attribute name
	 */
	private static String getAttributeName(Class<?> parameter) {
		String [] splitName = parameter.getName().split(Pattern.quote("."));
		String attributeName = splitName[splitName.length-1];
		attributeName = Character.toLowerCase(attributeName.charAt(0)) + attributeName.substring(1, attributeName.length());
		return attributeName;
	}
	
	
	/**
	 * Test if an attribute has already been injected
	 * @param instance, the instance containing the attribute
	 * @param parameter, the parameter corresponding dot attribute
	 * @return true if the attribute has not been injected and false otherwise
	 */
	private static boolean isAttributeNotAlreadyInjected(Object instance, Class<?> parameter) {
		// Get the attribute name
		String attributeName = getAttributeName(parameter);
	   
		   Object attribute = null;
		   try {
				Field field = instance.getClass().getDeclaredField(attributeName);
				
				// Change visibility to access to its implementation
				field.setAccessible(true);
				attribute = field.get(instance);
			} catch ( IllegalAccessException |NoSuchFieldException e) {
				e.printStackTrace();
			}
		   return attribute == null;
	}

	/**
	 * Find the constructor to use for injection
	 * @param c, the class where to search constructors
	 * @return the constructor found
	 * @throws MultipleInjectConstructorsFoundException
	 */
	@SuppressWarnings("rawtypes")
	private static ArrayList<Constructor> findConstructor(Class<? extends Object> c) throws MultipleInjectConstructorsFoundException {
		System.out.println("Looking for constructor for injection in " + c.getSimpleName());
		ArrayList<Constructor> annotatedConstructors = new ArrayList<Constructor>();
		Constructor<?>[] constructors = c.getDeclaredConstructors();
		
		for(Constructor constructor : constructors) {
			if(constructor.isAnnotationPresent(InjectConstructor.class)) {
				annotatedConstructors.add(constructor);
			}
		}
		if(annotatedConstructors.size() >1) {
			// Too many constructors found
			throw new MultipleInjectConstructorsFoundException();
		} else {
			if (annotatedConstructors.size() == 0) System.out.println("No contructor found on type "+ c.getSimpleName());
			// The only one existing constructor
			return annotatedConstructors;
		}
	}
}

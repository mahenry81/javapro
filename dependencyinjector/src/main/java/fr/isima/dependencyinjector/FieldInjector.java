package fr.isima.dependencyinjector;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import fr.isima.dependencyinjector.annotations.InjectField;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;

/**
 * Class used for injection of Fields
 *
 */
public class FieldInjector {
	
	private FieldInjector() {}
	
	/**
	 * Scan a class and determine which attribute needs to be instantiate thanks to the @InjectField
	 * 
	 * @param instance, the instance to scan
	 * @return the attributes which need to be instantiate
	 */
	public static List<Field> findFieldToInject(Class<?> instance){
		System.out.println("Looking for attributes on instance of " + instance.getSimpleName());
		List<Field> fieldsToInstanciate = new ArrayList<>();
		
		// Scan all the attribute of the class
		for (Field field : instance.getDeclaredFields()) {
			if (field.isAnnotationPresent(InjectField.class)) {
				fieldsToInstanciate.add(field);
			}
		}
		// There are no field to inject
		if (fieldsToInstanciate.isEmpty()) {
			 System.out.println("No attribute found to inject on type " + instance.getSimpleName());
		}
		return fieldsToInstanciate;
	}
	
	/**
	 * Instantiate all the attributes of a class with @InjectField
	 * 
	 * @param attributes, the attributes to instantiate
	 * @throws MultiplteDefaultImplFoundException 
	 * @throws MultiplteImplFoundException 
	 * @throws NoMatchingFoundException 
	 * @throws AbstractClassException 
	 */
	public static void instantiateAllAttributes(Object instance) throws NoMatchingFoundException, 
	MultiplteImplFoundException, MultiplteDefaultImplFoundException, AbstractClassException {
		System.out.println("go in field injection");
		Class<?> instanceToScan = instance.getClass();
		// Search all field to inject
		List<Field> attributes = findFieldToInject(instanceToScan);
		for (Field field : attributes) { // Instantiate all field to inject
			instanciateAttribute(field, instance);
		}
	}
	
	/**
	 * Instantiate a single attribute
	 * 
	 * @param attribute, the attribute to instantiate
	 * @param instance, the instance which contains the attribute
	 * @throws NoMatchingFoundException
	 * @throws MultiplteImplFoundException
	 * @throws MultiplteDefaultImplFoundException
	 * @throws AbstractClassException 
	 */
	public static void instanciateAttribute(Field attribute, Object instance) throws NoMatchingFoundException,
	MultiplteImplFoundException, MultiplteDefaultImplFoundException, AbstractClassException {
		System.out.println("instantiation of attribute "+ attribute.getName());
		// Create a new instance
		Object value;
		try {
			value = Injector.newInstance(attribute.getType());
			attribute.setAccessible(true);
			// Inject the instance in the attribute
			attribute.set(instance, value);
		} catch (IllegalArgumentException | IllegalAccessException |NoConstructorWithNoParametersFoundException | 
				MultipleInjectConstructorsFoundException e) {
			e.printStackTrace();
		}
	}

}

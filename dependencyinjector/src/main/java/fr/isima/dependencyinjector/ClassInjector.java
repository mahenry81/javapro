package fr.isima.dependencyinjector;

import java.util.ArrayList;
import java.util.List;

import fr.isima.dependencyinjector.annotations.Default;
import fr.isima.dependencyinjector.annotations.Singleton;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;

/**
 * Class used to create new instance depending on types
 *
 */
public class ClassInjector {
	
	/**
	 * Search if there are @default annotation for injection
	 * @param impls, the list of object which could have the default annotation
	 * @return The default implementation if found, otherwise the only one implementation without default annotation if found, otherwise an error otherwise
	 * @throws MultiplteDefaultImplFoundException
	 * @throws MultiplteImplFoundException
	 */
	static Class<?> searchDefaultImpl(List<Object> impls) throws MultiplteDefaultImplFoundException, MultiplteImplFoundException{
		ArrayList<Class<?>> defaultImpls = new ArrayList<>();
		Class<?> defaultImpl = null;
		
		for (Object impl: impls) {
			Class<?> implClass = (Class<?>) impl;
			if (implClass.isAnnotationPresent(Default.class)) {
				defaultImpls.add(implClass);
			}
		}
		// Too many default impl
		if (defaultImpls.size() > 1) {
			throw new MultiplteDefaultImplFoundException();
		} else {
			// Default impl doesn't exist
			if (defaultImpls.size() == 0) {
				// There are only one implementation
				if (impls.size() == 1) {
					defaultImpl = (Class<?>) impls.get(0);
				} else { 
					// There are many impl and no default
					throw new MultiplteImplFoundException();
				}
			} else { 
				// There are only one default
				defaultImpl = defaultImpls.get(0);
			}
		}
		return defaultImpl;
	}
	
	/**
	 * Create a new instance of an interface
	 * @param impls, the list of implementation of the interface which could be instantiated
	 * @return A new instance of the default implementation or the only one implementation if found, otherwise an error 
	 * @throws NoConstructorWithNoParametersFoundException
	 * @throws MultipleInjectConstructorsFoundException
	 * @throws MultiplteDefaultImplFoundException
	 * @throws MultiplteImplFoundException
	 * @throws AbstractClassException 
	 * @throws NoMatchingFoundException 
	 */
	static <T> T newImplInstance(List<Object> impls) throws NoConstructorWithNoParametersFoundException, MultipleInjectConstructorsFoundException, 
	MultiplteDefaultImplFoundException,MultiplteImplFoundException, AbstractClassException, NoMatchingFoundException {
		// Search implementation
		Class<?> instanceClass = ClassInjector.searchDefaultImpl(impls);
		System.out.println("default implementation: " + instanceClass.getSimpleName());
		
		return newClassInstance(instanceClass);
	}
	
	/**
	 * Create a new instance of a class
	 * @param classToInject, the list of class which could be instantiated
	 * @return The new instance created, which is injected by Setter or/and by constructor, or an error
	 * @throws MultiplteDefaultImplFoundException
	 * @throws MultiplteImplFoundException
	 * @throws NoConstructorWithNoParametersFoundException
	 * @throws MultipleInjectConstructorsFoundException
	 * @throws AbstractClassException 
	 * @throws NoMatchingFoundException 
	 */
	@SuppressWarnings("unchecked")
	static <T> T newClassInstance(Class<?> classToInject) throws MultiplteDefaultImplFoundException, MultiplteImplFoundException, 
	NoConstructorWithNoParametersFoundException, MultipleInjectConstructorsFoundException, AbstractClassException, NoMatchingFoundException {
		try {
			// Initialize new instance
			T newInstance = (T) Class.forName(classToInject.getName()).newInstance();
			
			//if it's not a singleton
			if (!classToInject.isAnnotationPresent(Singleton.class)) {

			
				// Do injection by setter if possible
				SetterInjection.injectBySetter(newInstance);
				
				// Do injection by constructor if possible
				newInstance = ConstructorInjection.injectByConstructor(newInstance);
				
				// Do injection by field if possible
				FieldInjector.instantiateAllAttributes(newInstance);
			} else { //singleton
				newInstance = SingletonInjector.newSingletonInstance(classToInject);
			}

			return newInstance;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | 
					IllegalArgumentException | SecurityException e) {
			if( e instanceof InstantiationException) throw new NoConstructorWithNoParametersFoundException();
			System.out.println("erreur: " +e);
		}
		return null;
		
	}

}

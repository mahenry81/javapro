package fr.isima.dependencyinjector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.HttpService;
import fr.isima.dependencyinjector.service.NewsService;
import fr.isima.dependencyinjector.serviceImpl.DarkWebHttpService;
import fr.isima.dependencyinjector.serviceImpl.RssNewsService;

public class SetterTest {
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
	
	/**
	 * Test to injection Object and Value with setter
	 */
	@Test
    public void test_SetterInjection_ObjectAndValue()  {
        try {
            Injector.register(HttpService.class,DarkWebHttpService.class);
            Injector.register(NewsService.class,RssNewsService.class);
            Injector.register(int.class,0);
            
            NewsService newsService = Injector.newInstance(NewsService.class);
            
            assertTrue(newsService.getHttpService() instanceof DarkWebHttpService);
            assertEquals(newsService.getSource(),"");
            assertEquals(newsService.getTestTypePrimitif(),0);
        } catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException |
        		NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException 
        		| AlreadyBindException | AbstractClassException e) {
        	e.printStackTrace();
        }
    }
	
}

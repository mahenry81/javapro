package fr.isima.dependencyinjector.service;

public interface ICours {
	public abstract ISorts getSorts();
	public abstract int getNbHours();
}

package fr.isima.dependencyinjector.service;

public interface NewsService {

	public HttpService getHttpService();

	public String getSource();

	public int getTestTypePrimitif();

}

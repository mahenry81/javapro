package fr.isima.dependencyinjector.service;

public interface IRoom {

	public abstract ICours getCours();
	
	public abstract int getNbSeats();
}

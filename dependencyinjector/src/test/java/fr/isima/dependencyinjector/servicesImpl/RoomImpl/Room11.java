package fr.isima.dependencyinjector.servicesImpl.RoomImpl;

import fr.isima.dependencyinjector.annotations.InjectConstructor;
import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.IRoom;

public class Room11 implements IRoom{

	private ICours iCours;
	private int nbSeats;
	
	@InjectConstructor
	public Room11(ICours iCours, int nbSeats) {
		this.iCours = iCours;
		this.nbSeats = nbSeats;
	}
	@Override
	public ICours getCours() {
		return this.iCours;		
	}
	@Override
	public int getNbSeats() {
		return this.nbSeats;
	}
}

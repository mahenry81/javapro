package fr.isima.dependencyinjector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.classes.Poudlard;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.IMaison;
import fr.isima.dependencyinjector.serviceImpl.maisonImpl.Gryffondor;

public class FieldTest {
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
	
	/**
	 * Test to inject attribute by fields
	 */
	@Test
	public void test_FieldInjection_Attribute() {
		try {
			Injector.register(IMaison.class, Gryffondor.class);
			Injector.register(int.class,0);
			 
			Poudlard poudlard = Injector.newInstance(Poudlard.class);
			
			assertEquals(0, poudlard.getNbEleve());
			assertTrue(poudlard.getMaison() instanceof Gryffondor);
			assertEquals(null, poudlard.getSc());
			
			
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}		
	}
}

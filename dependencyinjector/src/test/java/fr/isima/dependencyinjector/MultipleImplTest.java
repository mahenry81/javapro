package fr.isima.dependencyinjector;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.IBalais;
import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.IMaison;
import fr.isima.dependencyinjector.service.ISorts;
import fr.isima.dependencyinjector.serviceImpl.BalaisImpl.Nimbus2000;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Potions;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Sortileges;
import fr.isima.dependencyinjector.serviceImpl.maisonImpl.Gryffondor;
import fr.isima.dependencyinjector.serviceImpl.maisonImpl.Poufsouffle;
import fr.isima.dependencyinjector.serviceImpl.maisonImpl.Serdaigle;
import fr.isima.dependencyinjector.serviceImpl.maisonImpl.Serpentard;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.Accio;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.Allohomora;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.AvadaKedavra;

public class MultipleImplTest {
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
    
	/**
	 * Test injection of class with annotation @Default
	 */
    @Test 
    public void test_DefaultImplInjection() {
    	try {
			Injector.register(IMaison.class, Gryffondor.class);
			Injector.register(IMaison.class, Poufsouffle.class);
	    	Injector.register(IMaison.class, Serdaigle.class);
	    	Injector.register(IMaison.class, Serpentard.class);
	    	
	    	IMaison maison;
	    	maison = Injector.newInstance(IMaison.class);
	    	
	    	assertTrue(maison instanceof Gryffondor);
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				MultipleInjectConstructorsFoundException | NoConstructorWithNoParametersFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}  	
    }
    
    /**
     * Test injection fail when multiple implementation defined
     * @throws MultiplteImplFoundException
     */
    @SuppressWarnings("unused")
	@Test(expected = MultiplteImplFoundException.class)
    public void test_MultiplteImplFoundException() throws MultiplteImplFoundException {
    	try {
			Injector.register(ICours.class, Potions.class);
			Injector.register(ICours.class, Sortileges.class);
			
			ICours cours = Injector.newInstance(ICours.class);
		} catch (NoMatchingFoundException | MultipleInjectConstructorsFoundException | NoConstructorWithNoParametersFoundException |  
				MultiplteDefaultImplFoundException | AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
		
    }
    
    /**
     * Test injection fail when multiple implementation defined with annotation @Default
     * @throws MultiplteDefaultImplFoundException
     */
    @SuppressWarnings("unused")
	@Test(expected = MultiplteDefaultImplFoundException.class)
    public void test_MultipleDefaultImplFoundException() throws MultiplteDefaultImplFoundException {
    	try {
			Injector.register(ISorts.class, Accio.class);
			Injector.register(ISorts.class, Allohomora.class);
			Injector.register(ISorts.class, AvadaKedavra.class);
			
			ISorts sorts = Injector.newInstance(ISorts.class);
		} catch (NoMatchingFoundException | MultipleInjectConstructorsFoundException | MultiplteImplFoundException |
				NoConstructorWithNoParametersFoundException | AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Test injection when only on implementation defined
     */
    @Test
    public void test_NoDefaultButOnlyOneImpl() {
    	try {
			Injector.register(IBalais.class, Nimbus2000.class);
			
			IBalais balais = Injector.newInstance(IBalais.class);
			
			assertTrue(balais instanceof Nimbus2000);
		} catch (NoMatchingFoundException  | MultiplteImplFoundException | MultipleInjectConstructorsFoundException | 
				NoConstructorWithNoParametersFoundException | MultiplteDefaultImplFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
}

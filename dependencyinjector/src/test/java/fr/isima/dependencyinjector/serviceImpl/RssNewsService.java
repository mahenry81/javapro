package fr.isima.dependencyinjector.serviceImpl;

import fr.isima.dependencyinjector.annotations.InjectSetter;
import fr.isima.dependencyinjector.service.HttpService;
import fr.isima.dependencyinjector.service.NewsService;

public class RssNewsService implements NewsService{
	
	private String source;
	
	private HttpService httpService;
	
	private int testTypePrimitif;
	
	@InjectSetter
	public void setHttpService(HttpService httpService){
		this.httpService = httpService;
	}
	
	@InjectSetter
	public void setSource(String source) {
		this.source = source;
	}
	
	@InjectSetter
	public void setTestTypePrimitif(int testTypePrimitif) {
		this.testTypePrimitif = testTypePrimitif;
	}
	
	@Override
	public HttpService getHttpService() {
		return httpService;
	}
	
	@Override
	public String getSource() {
		return source;
	}
	
	@Override
	public int getTestTypePrimitif() {
		return testTypePrimitif;
	}

}

package fr.isima.dependencyinjector.serviceImpl.coursImpl;

import fr.isima.dependencyinjector.annotations.InjectConstructor;
import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.ISorts;

public class Metamorphose implements ICours {

	private ISorts iSorts;
	private int nbHours;
	
	public Metamorphose() {
		
	}
	
	@InjectConstructor
	public Metamorphose(ISorts iSorts, int nbHours) {
		this.iSorts = iSorts;
		this.nbHours = nbHours;
	}
	
	@InjectConstructor
	public Metamorphose(ISorts iSorts) {
		this.iSorts = iSorts;
	}
	
	@Override
	public ISorts getSorts() {
		return this.iSorts;
	}

	@Override
	public int getNbHours() {
		return this.nbHours;
	}

}

package fr.isima.dependencyinjector.serviceImpl.coursImpl;

import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.ISorts;

public class Botanique implements ICours{

	private ISorts iSorts;
	private int nbHours;
	
	public Botanique(ISorts iSorts, int nbHours) {
		this.iSorts = iSorts;
		this.nbHours = nbHours;
	}
	
	public Botanique() {
		
	}
	
	@Override
	public ISorts getSorts() {
		return this.iSorts;
	}

	@Override
	public int getNbHours() {
		return this.nbHours;
	}

}

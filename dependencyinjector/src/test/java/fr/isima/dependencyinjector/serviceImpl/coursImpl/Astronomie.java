package fr.isima.dependencyinjector.serviceImpl.coursImpl;

import fr.isima.dependencyinjector.annotations.InjectConstructor;
import fr.isima.dependencyinjector.annotations.InjectSetter;
import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.ISorts;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.AvadaKedavra;

public class Astronomie implements ICours {

	private ISorts iSorts;
	private int nbHours;
	
	public Astronomie() {
		
	}
	
	@InjectConstructor
	public Astronomie(ISorts iSorts, int nbHours) {
		this.iSorts = iSorts;
		this.nbHours = nbHours;
	}
	
	@InjectSetter
	public void setISorts(ISorts iSorts) {
		this.iSorts = iSorts;
		this.iSorts = new AvadaKedavra();
	}
	@InjectSetter
	public void setNbHours(int nbHours) {
		this.nbHours = nbHours + 10;
	}
	
	@Override
	public ISorts getSorts() {
		return this.iSorts;
	}

	@Override
	public int getNbHours() {
		return this.nbHours;
	}

	
}

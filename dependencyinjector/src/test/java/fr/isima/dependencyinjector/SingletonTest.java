package fr.isima.dependencyinjector;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.classes.SimpleClass;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.IBalais;
import fr.isima.dependencyinjector.serviceImpl.BalaisImpl.Nimbus2000;

public class SingletonTest {
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
	
	/**
	 * Test injection of a class with singleton
	 */
	@Test
	public void test_Singleton_SimpleClass() {
		try {
			SimpleClass simpleClass = Injector.newInstance(SimpleClass.class);			
			SimpleClass simpleClass2 = Injector.newInstance(SimpleClass.class);
			
			assertEquals(simpleClass, simpleClass2);
			
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException 
				| AbstractClassException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Test injection of a implementation of an interface with singleton
	 */
	@Test
	public void test_Singleton_ImplInterface() {
		try {
			Injector.register(IBalais.class, Nimbus2000.class);
			
			IBalais balais = Injector.newInstance(IBalais.class);			
			IBalais balais2 = Injector.newInstance(IBalais.class);
			
			assertEquals(balais, balais2);
			
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException |
				NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}	
	}

}

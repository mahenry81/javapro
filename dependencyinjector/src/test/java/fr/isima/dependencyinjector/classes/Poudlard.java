package fr.isima.dependencyinjector.classes;

import fr.isima.dependencyinjector.annotations.InjectField;
import fr.isima.dependencyinjector.service.IMaison;

public class Poudlard {
	
	@InjectField
	private IMaison maison;
	
	@InjectField
	public int nbEleve;
	
	public SimpleClass sc;
	
	public IMaison getMaison() {
		return maison;
	}
	
	public int getNbEleve() {
		return nbEleve;
	}
	
	public SimpleClass getSc() {
		return sc;
	}
	
	
	

}

package fr.isima.dependencyinjector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.ICours;
import fr.isima.dependencyinjector.service.IRoom;
import fr.isima.dependencyinjector.service.ISorts;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Astronomie;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Botanique;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Metamorphose;
import fr.isima.dependencyinjector.serviceImpl.coursImpl.Sortileges;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.AvadaKedavra;
import fr.isima.dependencyinjector.serviceImpl.sortImpl.Imperium;
import fr.isima.dependencyinjector.servicesImpl.RoomImpl.Room11;

public class ConstructorTest {
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
	
	/**
	 * Test injection of a implementation containing object and value
	 */
	@Test
    public void test_ConstructorInjection_ObjectAndValue() {
		try {
			Injector.register(ISorts.class, Imperium.class);
			Injector.register(ICours.class, Sortileges.class);
			Injector.register(int.class, 24);
			
			ICours icours = Injector.newInstance(ICours.class);
			
			assertTrue(icours.getSorts() instanceof Imperium);
			assertEquals(icours.getNbHours(),24);
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException |
				NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
	/**
	 * Test injection does not be done with constructor without annotation
	 */
    @Test
    public void test_ConstructorInjection_ConstructorWithoutAnnotation() {
		try {
			Injector.register(ISorts.class, Imperium.class);
			Injector.register(ICours.class, Botanique.class);
			Injector.register(int.class, 24);
			
			ICours icours = Injector.newInstance(ICours.class);
			
			assertNull(icours.getSorts());
			assertEquals(icours.getNbHours(),0);
			
		} catch (MultiplteImplFoundException | NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException |
				MultiplteDefaultImplFoundException | NoMatchingFoundException | AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Test fail injection when no constructor without parameters has not been defined
     * @throws NoConstructorWithNoParametersFoundException
     */
    @SuppressWarnings("unused")
	@Test(expected = NoConstructorWithNoParametersFoundException.class)
    public void test_ConstructorInjection_NoConstructorWithoutParametersDefined() throws NoConstructorWithNoParametersFoundException{
		try {
			Injector.register(ICours.class, Sortileges.class);
			Injector.register(IRoom.class, Room11.class);
			Injector.register(int.class, 30);
			
			IRoom iRoom = Injector.newInstance(IRoom.class);
			
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultipleInjectConstructorsFoundException |
				MultiplteDefaultImplFoundException | AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Test injection does not change object but only value
     */
    @Test
    public void test_ConstructorInjection_InjectionAlreadyDoneBySetterInjection(){
		try {
			Injector.register(ISorts.class, Imperium.class);
			Injector.register(ICours.class, Astronomie.class);
			Injector.register(int.class, 20);
			
			ICours icours = Injector.newInstance(ICours.class);
			assertTrue(icours.getSorts() instanceof AvadaKedavra);
			assertEquals(icours.getNbHours(),20);
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Test injection fail when to many constructor defined with the annotation @InjectConstructor
     * @throws MultipleInjectConstructorsFoundException
     */
    @SuppressWarnings("unused")
	@Test(expected = MultipleInjectConstructorsFoundException.class)
    public void test_ConstructorInjection_ClassWithMuplipleAnnotatedConstructors() throws MultipleInjectConstructorsFoundException{
		try {
			Injector.register(ISorts.class, Imperium.class);
			Injector.register(ICours.class, Metamorphose.class);
			Injector.register(int.class, 10);
			
			ICours icours = Injector.newInstance(ICours.class);
			
		} catch (NoMatchingFoundException | MultiplteImplFoundException | NoConstructorWithNoParametersFoundException |
				MultiplteDefaultImplFoundException | AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
}

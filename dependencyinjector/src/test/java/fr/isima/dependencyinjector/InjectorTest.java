package fr.isima.dependencyinjector;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fr.isima.dependencyinjector.classes.AbstractClass;
import fr.isima.dependencyinjector.classes.SimpleClass;
import fr.isima.dependencyinjector.exception.AbstractClassException;
import fr.isima.dependencyinjector.exception.AlreadyBindException;
import fr.isima.dependencyinjector.exception.MultipleInjectConstructorsFoundException;
import fr.isima.dependencyinjector.exception.MultiplteDefaultImplFoundException;
import fr.isima.dependencyinjector.exception.MultiplteImplFoundException;
import fr.isima.dependencyinjector.exception.NoConstructorWithNoParametersFoundException;
import fr.isima.dependencyinjector.exception.NoMatchingFoundException;
import fr.isima.dependencyinjector.service.HttpService;
import fr.isima.dependencyinjector.service.NewsService;
import fr.isima.dependencyinjector.serviceImpl.DarkWebHttpService;

public class InjectorTest {  
	
	@Before
	public void clearRegistry() {
		Injector.registry.clear();
	}
    
    /**
     * Test injection of implementation of an interface
     */
    @Test
    public void test_InterfaceInjection()  {
    	try {
			Injector.register(HttpService.class,DarkWebHttpService.class);
			HttpService httpService = Injector.newInstance(HttpService.class);
			
			assertTrue(httpService instanceof HttpService);
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				MultipleInjectConstructorsFoundException | NoConstructorWithNoParametersFoundException 
				| AlreadyBindException | AbstractClassException e) {
			e.printStackTrace();
		}	
    }
    
    /**
     * Test injection of a class
     */
    @Test
    public void test_ClassInjection()  {
    	try {
			SimpleClass classeSimple = Injector.newInstance(SimpleClass.class);
			assertTrue(classeSimple instanceof SimpleClass);
		} catch (NoMatchingFoundException | MultiplteImplFoundException | MultiplteDefaultImplFoundException | 
				MultipleInjectConstructorsFoundException 
				| NoConstructorWithNoParametersFoundException | AbstractClassException e) {
			e.printStackTrace();
		}
    }
    
    
    /**
     * Test injection fail when no implementation precised
     * @throws NoMatchingFoundException
     */
    @SuppressWarnings("unused")
	@Test(expected = NoMatchingFoundException.class)
    public void test_NoMatchingFoundException() throws NoMatchingFoundException {
			try {
	            NewsService newsService = Injector.newInstance(NewsService.class);
			} catch (MultiplteImplFoundException | MultiplteDefaultImplFoundException | MultipleInjectConstructorsFoundException |
					NoConstructorWithNoParametersFoundException | AbstractClassException e) {
				e.printStackTrace();
			}
    }
    
    
    @Test(expected = AlreadyBindException.class)
    public void test_AlreadyBindException() throws AlreadyBindException {
		Injector.register(HttpService.class,DarkWebHttpService.class);
		Injector.register(HttpService.class,DarkWebHttpService.class);
    }
    
    
    @SuppressWarnings("unused")
	@Test(expected = AbstractClassException.class)
    public void test_AbstractClassException() throws AbstractClassException {
    	try {
			AbstractClass abstractClass = Injector.newInstance(AbstractClass.class);
		} catch (MultiplteImplFoundException | NoMatchingFoundException | MultiplteDefaultImplFoundException
				| NoConstructorWithNoParametersFoundException | MultipleInjectConstructorsFoundException e) {
			e.printStackTrace();
		}
    }
}
